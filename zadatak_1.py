# Zadatak 1
# a. Dodelite varijabli sentence vrednost: "Mislim da je seminar lingvistike kul." Indeksacijom varijable sentence dodelite vrednost varijabli s1 "seminar lingvistike".
# b. Varijabli s2 takođe dodelite vrednost "seminar lingvistike", ali ovoga puta koristeći .split() komandu, indeksaciju i operaciju konkatenacije elemenata liste. Tip s2 na kraju treba da bude niska.

sentence = "Mislim da je seminar lingvistike kul."

# a.

s1 = sentence[13:32]
# ili
s1 = sentence[13:-5]
# ili
s1 = sentence[-24:-5]
# ili
s1 = sentence[13:len(sentence)-5]
# ili
s1 = sentence[-24:len(sentence)-5]

print(s1)

# b.

splitted_sentence = sentence.split(" ")
seminar = splitted_sentence[3]
lingvistike = splitted_sentence[4]
s2 = seminar + " " + lingvistike
# ili
splitted_sentence = sentence.split(" ")
s2 = splitted_sentence[3] + " " + splitted_sentence[4]
# ili
s2 = sentence.split(" ")[3] + " " + sentence.split(" ")[4]

print(s2)
print(type(s2))