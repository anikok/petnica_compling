dodole = "Oj, dodole, mili Bože\noj, dodo, dodole\nNaša doda Boga moli\noj, dodo, dodole\nSitna kiša da zarosi\noj, dodo, dodole\nNaša polja da potopi\noj, dodo, dodole\nI da rodi bel' pšenica\noj, dodo, dodole\nI kukuruz sa dva klasa\noj, dodo, dodole\nŠto molila umolila\noj, dodo, dodole\nSitna kiša zarosila\noj, dodo, dodole\nNaša polja potopila\noj, dodo, dodole\nI rodila bel' pšenica\noj, dodo, dodole\nI kukuruz sa dva klasa\noj, dodo, dodole"

# redovi od teksta
redovi = dodole.split("\n")

obrnuto = str() # obrnuto = ""
for i in range(len(redovi), 0, -1):
	red = redovi[i-1]
	# reči od redova
	reci = red.split(" ")
	for j in range(len(reci), 0, -1):
		rec = reci[j-1]
		rec = rec.strip(",.")
		# dodaj reč
		obrnuto += rec + " "
	obrnuto += "\n"

print(obrnuto)



'''
for red in redovi[::-1]:
	print(red)
'''

