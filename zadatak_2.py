# Zadatak 2
# Napravite strukturu podataka pod promenljivom petnica, tako da ako ukucate print(petnica["A"]) dobijete spisak seminara na A odeljenju Petnice (ANT, AHL, IST, DIZ, DRH, LIN, PSI), ako ukucate print(petnica["B"]) dobijete spisak seminara na B odeljenju Petnice (BIO, BMD, EBH, HEM, GEO), a ako ukucate print(petnica["C"]) dobijete spisak seminara na C odeljenju petnice (AST, FIZ, MAT, PFE, RAČ, TEH).

petnica = dict() # opciono

petnica = {"A": ["ANT", "AHL", "IST", "DIZ", "DRH", "LIN", "PSI"], "B": ["BIO", "BMD", "EBH", "HEM", "GEO"], "C": ["AST", "FIZ", "MAT", "PFE", "RAČ", "TEH"]}
# ili
petnica = {"A": {"ANT", "AHL", "IST", "DIZ", "DRH", "LIN", "PSI"}, "B": {"BIO", "BMD", "EBH", "HEM", "GEO"}, "C": {"AST", "FIZ", "MAT", "PFE", "RAČ", "TEH"}}

print(petnica["A"])
print(petnica["B"])
print(petnica["C"])